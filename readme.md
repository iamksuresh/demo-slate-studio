# About Attachments :
1. ZIP : Artizt-Api
-   Postman collection for Api is in 'Artizt Manager.postman_collection.json' file

2. ZIP : React+Google+Maps
    Project Description 
    - Show the list of restaurants on Google map
    - Project using React web hooks and Express server
    - Markers should show images/icons and show if restaurant is open or closed
    - Favourite's list to add restaurant
    - Context Api is used between favourite's list and google maps

Please use Readme.md for installation details